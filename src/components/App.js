import React from "react";
import UserCreate from "./UserCreate";
import { LanguageStore } from "../contexts/LanguageContext";
import ColorContext from "../contexts/ColorContext";
import LanguageSelector from "../components/LanguageSelector";

class App extends React.Component {
  state = { color: "red" };

  onColorChange = (color) => {
    this.setState({ color });
  };

  render() {
    return (
      <div className="ui container">
        <LanguageStore>
          <LanguageSelector onColorChange={this.onColorChange} />
          <ColorContext.Provider value={this.state.color}>
            <UserCreate />
          </ColorContext.Provider>
        </LanguageStore>
      </div>
    );
  }
}

export default App;
