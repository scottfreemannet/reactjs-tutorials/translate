import React from "react";
import LanguageContext from "../contexts/LanguageContext";

class LanguageSelector extends React.Component {
  static contextType = LanguageContext;

  render() {
    return (
      <div>
        Select a language:&nbsp;
        <i
          className="flag gb"
          onClick={() => this.context.onLanguageChange("english")}
        />
        <i
          className="flag nl"
          onClick={() => this.context.onLanguageChange("dutch")}
        />
        //
        <i
          className="circle icon red"
          onClick={() => this.props.onColorChange("red")}
        />
        <i
          className="circle icon blue"
          onClick={() => this.props.onColorChange("blue")}
        />
        <i
          className="circle icon green"
          onClick={() => this.props.onColorChange("green")}
        />
      </div>
    );
  }
}

export default LanguageSelector;
